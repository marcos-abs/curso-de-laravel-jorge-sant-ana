# Para a Instalação do Ambiente

## Atualizado em

Tuesday, 19 July 2022 08:44:42

### Versões

Utilizar sempre as versões deste documento para manter a compatibilidade do conteúdo do curso com a aplicação.

* Laravel 7.x
* PHP 7.4

#### Antes de criar um projeto

```bash
sudo apt-get install php7.4-mbstring
sudo apt-get install php7.4-dom
sudo apt-get install php7.4-zip

```

#### Para criar um projeto via composer

Aula: 13. Iniciando um projeto Laravel (Via Composer)

```bash
 composer create-project --prefer-dist laravel/laravel blog
```

![linha-de-comando-executada](20220719094110.png)

#### Para criar um projeto via installer

```bash
composer global require laravel/installer
```

![resultado-composer-1-de-2](20220719095146.png)
![resultado-composer-2-de-2](20220719095329.png)

**Detalhe importante:** o local de instalação o pacote installer do Laravel.
![resultado-local-pacote-installer](20220719095858.png)
Esta informação será utilizada na configuração dos demais pacotes do Laravel.

**Agora e necessário configurar a variável PATH** no sistema para que o Laravel funcione  em qualquer do shell

* Aula 14. Instalando o Laravel Installer

**Veja os exemplos da documentação** do site no Laravel
![exemplos-variáveis-ambiente](20220719100201.png)

#### Para adicionar o laravel a variável PATH do sistema

* editar o arquivo .bashrc do diretório ddo usuario do sistema
![nano](20220719100955.png)

* reabrir a sessão do usuario para que a variável carregue

* validar se a configuração foi bem sucedida
![versão](20220719101418.png)

#### Para criar um projeto via laravel installer (continuação)

Aula 15. Iniciando um projeto Laravel (Via Laravel Installer)

```bash
laravel new projeto_laravel_via_installer
```

A execução do **comando:**
![execução 1 de 2](20220719125055.png)
![execução 2 de 2](20220719125225.png)

Para a validação é necessário subir o servidor do laravel via **linha de comando**:

```bash
php -S localhost:8000
```

A **validação** do servidor em execução:
![validação](20220719125531.png)
